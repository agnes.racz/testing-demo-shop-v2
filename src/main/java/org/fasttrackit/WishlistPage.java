package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$;

public class WishlistPage {
    private final ElementsCollection wishlistProducts = $$(".card");

    public int getWishlistProductsNumber(){
        return wishlistProducts.size();
    }
}
