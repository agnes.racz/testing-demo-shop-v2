package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class Page {

    private static final String URL = "https://fasttrackit-int.netlify.app/#/";
    private final SelenideElement pageTitle = $(".subheader-container");

    private final SelenideElement searchBox = $("#input-search");

    private final SelenideElement searchButton = $(".btn-light");

    private final SelenideElement sortButton = $(".sort-products-select");
    private final SelenideElement sortZAOption = $("[value='za']");
    private final SelenideElement sortAZOption = $("[value='az']");
    private final SelenideElement sortLoHiOption = $("[value='lohi']");
    private final SelenideElement sortHiLoOption = $("[value='hilo']");

    public void openHomepage() {
        open(URL);
    }

    public String getPageTitle() {
        return pageTitle.text();
    }

    public void clickOnBackButton() {
        back();
    }

    public void typeInSearchQuery(String searchQuery) {
        searchBox.type(searchQuery);
    }

    public void clickOnSearchButton() {
        searchButton.click();
    }

    public void hitEnterOnSearchButton(){
        searchButton.pressEnter();
    }

    public void clickOnSortButton(){
        sortButton.click();
    }

    public void clickOnSortAZOption(){
        sortAZOption.click();
    }
    public void clickOnSortZAOption(){
        sortZAOption.click();
    }
    public void clickOnSortLoHiOption(){
        sortLoHiOption.click();
    }    public void clickOnSortHiLoOption(){
        sortHiLoOption.click();
    }


}