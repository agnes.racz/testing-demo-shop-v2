package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage {
    private final ElementsCollection cartProducts = $$(".fa-trash");
    private final SelenideElement emptyCartMessage = $(".text-center");
    private final SelenideElement itemsTotal = $("tr:first-of-type .amount");
    private final SelenideElement total = $(".amount-total");
    private final SelenideElement continueShoppingButton = $(".fa-angle-left");
    private final SelenideElement checkoutButton = $(".fa-angle-right");

    public int getNumberOfProductsInCart() {
        return cartProducts.size();
    }

    public String getEmptyCartMessage() {
        return emptyCartMessage.text();
    }

    public Double getItemsTotal(){
        String total = itemsTotal.text();
        return Double.valueOf(total.substring(1));
    }

    public Double getTotal(){
        String checkoutTotal = total.text();
        return Double.valueOf(checkoutTotal.substring(8));
    }

    public void clickOnContinueShoppingButton(){
        continueShoppingButton.click();
    }
    public void clickOnCheckoutButton(){
        checkoutButton.click();
    }
}
