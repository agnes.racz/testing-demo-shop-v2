package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CartProduct {
    private final SelenideElement productRow;
    private final SelenideElement minusButton;
    private final SelenideElement productQty;
    private final SelenideElement plusButton;
    private final SelenideElement productPrice;
    private final SelenideElement totalPricePerProduct;
    private final SelenideElement productLink;
    private final SelenideElement trashCanButton;


    public CartProduct(String productId) {
        String productIdSelector = String.format("#item_%s_title_link", productId);
        this.productLink = $(productIdSelector);
        this.productRow = productLink.parent().parent();
        this.minusButton = productRow.$("[data-icon='minus-circle']");
        this.productQty = productRow.$("div:first-of-type");
        this.plusButton = productRow.$("[data-icon='plus-circle']");
        this.trashCanButton = productRow.$(".fa-trash");
        this.productPrice = productRow.$("div:nth-of-type(2)");
        this.totalPricePerProduct = productRow.$("div:nth-of-type(3)");
    }

    public String getProductName() {
        return productLink.text();
    }

    public void clickOnMinusButton() {
        minusButton.click();
    }

    public void clickOnPlusButton() {
        plusButton.click();
    }

    public void clickOnTrashCanButton() {
        trashCanButton.click();
    }

    public Double getProductPrice() {
        String price = productPrice.text();
        return Double.valueOf(price.substring(1));
    }

    public String getProductQuantity() {
        return productQty.text();
    }

    public double getTotalPricePerProduct(){
        String price = totalPricePerProduct.text();
        return Double.parseDouble(price.substring(1));
    }
}
