package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductCard {
    private final SelenideElement cardLink;
    private final SelenideElement cardBody;
    private final SelenideElement cardFooter;
    private final SelenideElement addToCartButton;
    private final SelenideElement addToWishlistButton;
    private final SelenideElement removeFromWishlistButton;
    private final String productName;

    public ProductCard(String productId) {
        String productIdSelector = String.format("[href='#/product/%s']", productId);
        this.cardLink = $(productIdSelector);
        this.productName = cardLink.text();
        this.cardBody = cardLink.parent();
        this.cardFooter = cardBody.sibling(0);
        this.addToCartButton = cardFooter.$(".fa-cart-plus");
        this.addToWishlistButton = cardFooter.$(".fa-heart");
        this.removeFromWishlistButton = cardFooter.$(".fa-heart-broken");

    }

    public void clickOnProductName() {
        cardLink.click();
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }

    public void clickOnAddToWishlistButton() {
        addToWishlistButton.click();
    }

    public boolean isRemoveFromWishlistButtonDisplayed() {
        return removeFromWishlistButton.isDisplayed();
    }

    public void clickOnRemoveFromWishlistButton() {
        removeFromWishlistButton.click();
    }

    public boolean isAddToWishlistButtonDisplayed() {
        return addToWishlistButton.isDisplayed();
    }
}
