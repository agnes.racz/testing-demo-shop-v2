package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement usernameField = $("#user-name");
    private final SelenideElement passwordField = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement errorMessage = $(".error");


    public String getModalTitle() {
        return modalTitle.text();
    }

    public void clickOnCloseButton() {
        closeButton.click();
    }

    public void typeInUsername(String username) {
        usernameField.type(username);
    }

    public void typeInPassword(String password) {
        passwordField.type(password);
    }

    public void clickOnLoginButton() {
        loginButton.click();
    }

    public String getErrorMessage(){
        return errorMessage.text();
    }
}
