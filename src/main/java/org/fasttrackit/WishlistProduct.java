package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WishlistProduct {

    private final SelenideElement productLink;
    private final SelenideElement cardBody;
    private final SelenideElement removeFromWishlistButton;

    public WishlistProduct(String productId) {
        String productIdSelector = String.format("[href='#/product/%s']", productId);
        this.productLink = $(productIdSelector);
        this.cardBody = productLink.parent();
        this.removeFromWishlistButton = cardBody.sibling(0).$(".fa-heart-broken");
    }

    public String getName() {
        return productLink.text();
    }

    public void clickOnRemoveFromWishlistButton() {
        removeFromWishlistButton.click();
    }
}


