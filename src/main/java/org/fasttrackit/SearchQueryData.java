package org.fasttrackit;

public class SearchQueryData {

    private final String searchQuery;
    private final int expectedProductsNumber;

    public SearchQueryData(String searchQuery, int number) {
        this.searchQuery = searchQuery;
        this.expectedProductsNumber = number;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public int getExpectedProductsNumber() {
        return expectedProductsNumber;
    }

}
