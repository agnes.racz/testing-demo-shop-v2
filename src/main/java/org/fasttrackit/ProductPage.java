package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductPage {
    private final SelenideElement productPrice = $(".text-center p:first-of-type");

    public Double getProductPrice() {
        String price = productPrice.text();
        return Double.valueOf(price.substring(0, price.length()-4));
    }
}

