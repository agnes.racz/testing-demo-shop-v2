package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$;

public class SearchResultPage {

    private final ElementsCollection returnedProductsName = $$(".card-link");

    public int getReturnedProductsNumber() {
        return returnedProductsName.size();
    }

    public boolean areResultsContainingQuery(String query) {
        boolean contains = false;
        for (String name : returnedProductsName.texts()) {
            name = name.toLowerCase();
            if (name.contains(query)) {
                contains = true;
            } else {
                contains = false;
                break;
            }
        }
        return contains;
    }
}
