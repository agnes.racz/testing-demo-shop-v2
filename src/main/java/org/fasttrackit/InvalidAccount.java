package org.fasttrackit;

public class InvalidAccount extends Account{
    private final String errorMessage;

    public InvalidAccount(String username, String password, String errorMessage) {
        super(username, password);
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage(){
        return this.errorMessage;
    }
}
