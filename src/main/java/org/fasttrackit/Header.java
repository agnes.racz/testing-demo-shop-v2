package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {

    private final SelenideElement logo = $(".brand-logo");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement cartBadge = cartIcon.sibling(0);
    private final SelenideElement wishlistIcon = $("[href='#/wishlist']");
    private final SelenideElement wishlistBadge = wishlistIcon.$(".shopping_cart_badge");

    private final SelenideElement greetingsMessage = wishlistIcon.sibling(0);
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement logOutIcon = $(".fa-sign-out-alt");
    private final ElementsCollection badges = $$(".shopping_cart_badge");

    public void clickOnCartIcon() {
        cartIcon.click();
    }

    public void clickOnWishlistIcon() {
        wishlistIcon.click();
    }

    public void clickOnLoginIcon() {
        loginIcon.click();
    }

    public void clickOnLogo() {
        logo.click();
    }

    public boolean isCartBadgeDisplayed() {
        return cartBadge.isDisplayed();
    }

    public String getCartBadgeValue() {
        return cartBadge.text();
    }

    public boolean isWishlistBadgeDisplayed() {
        return wishlistBadge.isDisplayed();
    }

    public String getWishlistBadgeValue() {
        return wishlistBadge.text();
    }

    public int getNumberOfBadges() {
        return badges.size();
    }

    public String getGreetingsMessage() {
        return greetingsMessage.text();
    }

    public boolean isLogOutIconDisplayed(){
        return logOutIcon.isDisplayed();
    }
}
