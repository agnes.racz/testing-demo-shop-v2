package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class ProductListingPage {
    private ElementsCollection productNameLinks = $$(".card-link");
    private final ElementsCollection productPrices = $$(".card-footer .card-text span");

    public boolean areProductsSortedAZ(){
        List<String> names = productNameLinks.texts();
        return Ordering.natural().isOrdered(names);
    }

    public boolean areProductsSortedZA(){
        List<String> names = productNameLinks.texts();
        return Ordering.natural().reverse().isOrdered(names);
    }

    public boolean areProductsSortedByPriceLoHi() {
        List<String> pricesText = productPrices.texts();
        List<Double> prices = new ArrayList<>();
        for (String price : pricesText) {
            prices.add(Double.valueOf(price));
        }
        return Ordering.natural().isOrdered(prices);
    }

    public boolean areProductsSortedByPriceHiLo() {
        List<String> pricesText = productPrices.texts();
        List<Double> prices = new ArrayList<>();
        for (String price : pricesText) {
            prices.add(Double.valueOf(price));
        }
        return Ordering.natural().reverse().isOrdered(prices);
    }
}
