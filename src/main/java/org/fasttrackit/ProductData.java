package org.fasttrackit;

public class ProductData {
    private final String id;
    private final String name;
    private final String price;
    private final double doublePrice;

    public ProductData(String id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.doublePrice = Double.parseDouble(this.price)*2;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return Double.parseDouble(price);
    }

    public Double getDoublePrice(){
        return doublePrice;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
