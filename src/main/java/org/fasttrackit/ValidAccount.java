package org.fasttrackit;

public class ValidAccount extends Account {
    private final String greetingMessage;

    public ValidAccount(String username, String password) {
        super(username, password);
        this.greetingMessage = "Hi " + username + "!";
    }

    public String getGreetingMessage() {
        return greetingMessage;
    }

}
