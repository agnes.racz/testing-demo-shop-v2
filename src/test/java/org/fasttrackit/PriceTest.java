package org.fasttrackit;

import com.codeborne.selenide.commands.Val;
import org.fasttrackit.dataprovider.AccountAndProductDataProvider;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;

public class PriceTest {
    ProductPage productPage = new ProductPage();
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    LoginModal loginModal = new LoginModal();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void product_page_shows_correct_price(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnProductName();
        assertEquals(productPage.getProductPrice(), productData.getPrice(), "Product price must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void product_added_to_cart_has_correct_price(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        assertEquals(cartProduct.getProductPrice(), productData.getPrice(), "Product price must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void product_added_to_cart_has_correct_total_price_per_product(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        assertEquals(cartProduct.getTotalPricePerProduct(), productData.getPrice(), "Product price must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void items_total_is_equal_to_product_price_when_cart_has_one_product(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        assertEquals(cartPage.getItemsTotal(), productData.getPrice(), "Items Total must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void total_price_is_equal_to_product_price_when_cart_has_one_product(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        assertEquals(cartPage.getTotal(), productData.getPrice(), "Total must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void price_doubles_when_guest_is_clicking_on_plus_button_on_a_product_in_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnPlusButton();
        assertEquals(cartProduct.getTotalPricePerProduct(), productData.getDoublePrice(), "Product Quantity must be: " + productData.getDoublePrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void items_total_doubles_when_guest_is_clicking_on_plus_button_on_a_product_in_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnPlusButton();
        assertEquals(cartPage.getItemsTotal(), productData.getDoublePrice(), "Items Total must be: " + productData.getDoublePrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void total_doubles_when_guest_is_clicking_on_plus_button_on_a_product_in_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnPlusButton();
        assertEquals(cartPage.getTotal(), productData.getDoublePrice(), "Total must be: " + productData.getDoublePrice());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "two_products")
    public void items_total_shows_correct_total_when_two_products_are_added_to_cart(ProductData productData1, ProductData productData2) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        Double expectedTotal = productData1.getPrice() + productData2.getPrice();
        assertEquals(cartPage.getItemsTotal(), expectedTotal, "Items Total must be: " + expectedTotal);
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "two_products")
    public void total_shows_correct_total_when_two_products_are_added_to_cart(ProductData productData1, ProductData productData2) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        Double expectedTotal = productData1.getPrice() + productData2.getPrice();
        assertEquals(cartPage.getTotal(), expectedTotal, "Total must be: " + expectedTotal);
    }
    //--------------
    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void product_page_shows_correct_price_for_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnProductName();
        assertEquals(productPage.getProductPrice(), productData.getPrice(), "Product price must be: " + productData.getPrice());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void product_added_to_cart_by_logged_in_user_has_correct_price(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        assertEquals(cartProduct.getProductPrice(), productData.getPrice(), "Product price must be: " + productData.getPrice());
    }

}
