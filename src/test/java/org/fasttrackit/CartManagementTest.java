package org.fasttrackit;

import io.qameta.allure.internal.shadowed.jackson.databind.jsontype.PolymorphicTypeValidator;
import org.fasttrackit.dataprovider.AccountAndProductDataProvider;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;

public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    LoginModal loginModal = new LoginModal();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void cart_contains_only_one_item_added_by_guest(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        assertEquals(cartPage.getNumberOfProductsInCart(), 1, "Total number of products must be 1");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void cart_contains_item_added_by_guest(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        assertEquals(cartProduct.getProductName(), productData.getName(), "Product name must be: " + productData.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "three_products")
    public void cart_contains_three_items_added_by_guest(ProductData productData1, ProductData productData2, ProductData productData3) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData1.getId());
        assertEquals(cartProduct.getProductName(), productData1.getName(), "Product name must be: " + productData1.getName());
        cartProduct = new CartProduct(productData2.getId());
        assertEquals(cartProduct.getProductName(), productData2.getName(), "Product name must be: " + productData2.getName());
        cartProduct = new CartProduct(productData3.getId());
        assertEquals(cartProduct.getProductName(), productData3.getName(), "Product name must be: " + productData3.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "fourteen_products")
    public void cart_contains_14_items_added_by_guest(List<ProductData> productData) {
        ProductCard productCard = new ProductCard(productData.get(0).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(1).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(2).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(3).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(4).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(5).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(6).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(7).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(8).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(9).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(10).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(11).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(12).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(13).getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.get(0).getId());
        assertEquals(cartProduct.getProductName(), productData.get(0).getName(), "Product name must be: " + productData.get(0).getName());
        cartProduct = new CartProduct(productData.get(1).getId());
        assertEquals(cartProduct.getProductName(), productData.get(1).getName(), "Product name must be: " + productData.get(1).getName());
        cartProduct = new CartProduct(productData.get(2).getId());
        assertEquals(cartProduct.getProductName(), productData.get(2).getName(), "Product name must be: " + productData.get(2).getName());
        cartProduct = new CartProduct(productData.get(3).getId());
        assertEquals(cartProduct.getProductName(), productData.get(3).getName(), "Product name must be: " + productData.get(3).getName());
        cartProduct = new CartProduct(productData.get(4).getId());
        assertEquals(cartProduct.getProductName(), productData.get(4).getName(), "Product name must be: " + productData.get(4).getName());
        cartProduct = new CartProduct(productData.get(5).getId());
        assertEquals(cartProduct.getProductName(), productData.get(5).getName(), "Product name must be: " + productData.get(5).getName());
        cartProduct = new CartProduct(productData.get(6).getId());
        assertEquals(cartProduct.getProductName(), productData.get(6).getName(), "Product name must be: " + productData.get(6).getName());
        cartProduct = new CartProduct(productData.get(7).getId());
        assertEquals(cartProduct.getProductName(), productData.get(7).getName(), "Product name must be: " + productData.get(7).getName());
        cartProduct = new CartProduct(productData.get(8).getId());
        assertEquals(cartProduct.getProductName(), productData.get(8).getName(), "Product name must be: " + productData.get(8).getName());
        cartProduct = new CartProduct(productData.get(9).getId());
        assertEquals(cartProduct.getProductName(), productData.get(9).getName(), "Product name must be: " + productData.get(9).getName());
        cartProduct = new CartProduct(productData.get(10).getId());
        assertEquals(cartProduct.getProductName(), productData.get(10).getName(), "Product name must be: " + productData.get(10).getName());
        cartProduct = new CartProduct(productData.get(11).getId());
        assertEquals(cartProduct.getProductName(), productData.get(11).getName(), "Product name must be: " + productData.get(11).getName());
        cartProduct = new CartProduct(productData.get(12).getId());
        assertEquals(cartProduct.getProductName(), productData.get(12).getName(), "Product name must be: " + productData.get(12).getName());
        cartProduct = new CartProduct(productData.get(13).getId());
        assertEquals(cartProduct.getProductName(), productData.get(13).getName(), "Product name must be: " + productData.get(13).getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_delete_product_from_cart_containing_one_item_by_clicking_on_minus_button(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnMinusButton();
        assertEquals(cartPage.getEmptyCartMessage(), "How about adding some products in your cart?", "Message must be: How about adding some products in your cart?");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_delete_product_from_cart_containing_one_item_by_clicking_on_trashCan_button(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnTrashCanButton();
        assertEquals(cartPage.getEmptyCartMessage(), "How about adding some products in your cart?", "Message must be: How about adding some products in your cart?");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "two_products")
    public void guest_can_delete_one_product_from_cart_containing_two_items_by_clicking_on_minus_button(ProductData productData1, ProductData productData2) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct1 = new CartProduct(productData1.getId());
        CartProduct cartProduct2 = new CartProduct(productData2.getId());
        cartProduct1.clickOnMinusButton();
        assertEquals(cartPage.getNumberOfProductsInCart(), 1, "Total number of products in cart must be 1");
        assertEquals(cartProduct2.getProductName(), productData2.getName(), "The remaining product's name must be: " + productData2.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "two_products")
    public void guest_can_delete_one_product_from_cart_containing_two_items_by_clicking_on_trashCan_button(ProductData productData1, ProductData productData2) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct1 = new CartProduct(productData1.getId());
        CartProduct cartProduct2 = new CartProduct(productData2.getId());
        cartProduct1.clickOnTrashCanButton();
        assertEquals(cartPage.getNumberOfProductsInCart(), 1, "Total number of products in cart must be 1");
        assertEquals(cartProduct2.getProductName(), productData2.getName(), "The remaining product's name must be: " + productData2.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_change_the_quantity_of_a_product_from_cart_by_clicking_on_plus_button(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnPlusButton();
        assertEquals(cartProduct.getProductQuantity(), "2", "Product Quantity must be 2");
    }

    //--------
    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void cart_contains_only_one_item_added_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        assertEquals(cartPage.getNumberOfProductsInCart(), 1, "Total number of products must be 1");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void cart_contains_item_added_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        assertEquals(cartProduct.getProductName(), productData.getName(), "Product name must be: " + productData.getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_three_products")
    public void cart_contains_three_items_added_by_logged_in_user(ValidAccount account, ProductData productData1, ProductData productData2, ProductData productData3) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData1.getId());
        assertEquals(cartProduct.getProductName(), productData1.getName(), "Product name must be: " + productData1.getName());
        cartProduct = new CartProduct(productData2.getId());
        assertEquals(cartProduct.getProductName(), productData2.getName(), "Product name must be: " + productData2.getName());
        cartProduct = new CartProduct(productData3.getId());
        assertEquals(cartProduct.getProductName(), productData3.getName(), "Product name must be: " + productData3.getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_fourteen_products")
    public void cart_contains_14_items_added_by_logged_in_user(ValidAccount account, List<ProductData> productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.get(0).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(1).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(2).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(3).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(4).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(5).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(6).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(7).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(8).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(9).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(10).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(11).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(12).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(13).getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.get(0).getId());
        assertEquals(cartProduct.getProductName(), productData.get(0).getName(), "Product name must be: " + productData.get(0).getName());
        cartProduct = new CartProduct(productData.get(1).getId());
        assertEquals(cartProduct.getProductName(), productData.get(1).getName(), "Product name must be: " + productData.get(1).getName());
        cartProduct = new CartProduct(productData.get(2).getId());
        assertEquals(cartProduct.getProductName(), productData.get(2).getName(), "Product name must be: " + productData.get(2).getName());
        cartProduct = new CartProduct(productData.get(3).getId());
        assertEquals(cartProduct.getProductName(), productData.get(3).getName(), "Product name must be: " + productData.get(3).getName());
        cartProduct = new CartProduct(productData.get(4).getId());
        assertEquals(cartProduct.getProductName(), productData.get(4).getName(), "Product name must be: " + productData.get(4).getName());
        cartProduct = new CartProduct(productData.get(5).getId());
        assertEquals(cartProduct.getProductName(), productData.get(5).getName(), "Product name must be: " + productData.get(5).getName());
        cartProduct = new CartProduct(productData.get(6).getId());
        assertEquals(cartProduct.getProductName(), productData.get(6).getName(), "Product name must be: " + productData.get(6).getName());
        cartProduct = new CartProduct(productData.get(7).getId());
        assertEquals(cartProduct.getProductName(), productData.get(7).getName(), "Product name must be: " + productData.get(7).getName());
        cartProduct = new CartProduct(productData.get(8).getId());
        assertEquals(cartProduct.getProductName(), productData.get(8).getName(), "Product name must be: " + productData.get(8).getName());
        cartProduct = new CartProduct(productData.get(9).getId());
        assertEquals(cartProduct.getProductName(), productData.get(9).getName(), "Product name must be: " + productData.get(9).getName());
        cartProduct = new CartProduct(productData.get(10).getId());
        assertEquals(cartProduct.getProductName(), productData.get(10).getName(), "Product name must be: " + productData.get(10).getName());
        cartProduct = new CartProduct(productData.get(11).getId());
        assertEquals(cartProduct.getProductName(), productData.get(11).getName(), "Product name must be: " + productData.get(11).getName());
        cartProduct = new CartProduct(productData.get(12).getId());
        assertEquals(cartProduct.getProductName(), productData.get(12).getName(), "Product name must be: " + productData.get(12).getName());
        cartProduct = new CartProduct(productData.get(13).getId());
        assertEquals(cartProduct.getProductName(), productData.get(13).getName(), "Product name must be: " + productData.get(13).getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_delete_product_from_cart_containing_one_item_by_clicking_on_minus_button(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnMinusButton();
        assertEquals(cartPage.getEmptyCartMessage(), "How about adding some products in your cart?", "Message must be: How about adding some products in your cart?");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_delete_product_from_cart_containing_one_item_by_clicking_on_trashCan_button(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnTrashCanButton();
        assertEquals(cartPage.getEmptyCartMessage(), "How about adding some products in your cart?", "Message must be: How about adding some products in your cart?");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_change_the_quantity_of_a_product_from_cart_by_clicking_on_plus_button(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnPlusButton();
        assertEquals(cartProduct.getProductQuantity(), "2", "Product Quantity must be 2");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_click_on_continue_shopping_from_cart_page_after_adding_one_product(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        cartPage.clickOnContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Products");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_three_products")
    public void logged_in_user_can_click_on_continue_shopping_from_cart_page_after_adding_three_products(ValidAccount account, ProductData productData1, ProductData productData2, ProductData productData3) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        cartPage.clickOnContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Products");
    }
    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_two_products")
    public void logged_in_user_can_add_one_more_product_after_returning_to_Products_from_cart(ValidAccount account, ProductData productData1, ProductData productData2) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard1 = new ProductCard(productData1.getId());
        productCard1.clickOnAddToCartButton();
        header.clickOnCartIcon();
        cartPage.clickOnContinueShoppingButton();
        ProductCard productCard2 = new ProductCard(productData2.getId());
        productCard2.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData1.getId());
        assertEquals(cartProduct.getProductName(), productData1.getName(), "Product name must be: " + productData1.getName());
        cartProduct = new CartProduct(productData2.getId());
        assertEquals(cartProduct.getProductName(), productData2.getName(), "Product name must be: " + productData2.getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_click_on_checkout_after_adding_one_product_to_cart(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        cartPage.clickOnCheckoutButton();
        assertEquals(page.getPageTitle(), "Your information", "Page title must be: Products");
    }

}