package org.fasttrackit;

import org.fasttrackit.dataprovider.AccountDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertTrue;

public class ProductSortingTest {
    Page page = new Page();
    Header header = new Header();
    LoginModal loginModal = new LoginModal();
    ProductListingPage productListingPage = new ProductListingPage();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test
    public void guest_can_sort_products_by_name_from_a_to_z() {
        page.clickOnSortButton();
        page.clickOnSortAZOption();
        assertTrue(productListingPage.areProductsSortedAZ(), "Products must be sorted by name A to Z");
    }

    @Test
    public void guest_can_sort_products_by_name_from_z_to_a() {
        page.clickOnSortButton();
        page.clickOnSortZAOption();
        assertTrue(productListingPage.areProductsSortedZA(), "Products must be sorted by name Z to A");
    }

    @Test
    public void guest_can_sort_products_by_name_from_a_to_z_after_sorting_from_z_a() {
        page.clickOnSortButton();
        page.clickOnSortZAOption();
        page.clickOnSortAZOption();
        assertTrue(productListingPage.areProductsSortedAZ(), "Products must be sorted by name A to Z");
    }

    @Test
    public void guest_can_sort_products_by_price_from_Lo_to_Hi() {
        page.clickOnSortButton();
        page.clickOnSortLoHiOption();
        assertTrue(productListingPage.areProductsSortedByPriceLoHi(), "Products must be sorted by price Low to High");
    }

    @Test
    public void guest_can_sort_products_by_price_from_Hi_to_Lo() {
        page.clickOnSortButton();
        page.clickOnSortHiLoOption();
        assertTrue(productListingPage.areProductsSortedByPriceHiLo(), "Products must be sorted by price High to Low");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void logged_in_user_can_sort_products_by_name_from_a_to_z(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.clickOnSortButton();
        page.clickOnSortAZOption();
        assertTrue(productListingPage.areProductsSortedAZ(), "Products must be sorted by name A to Z");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void logged_in_user_can_sort_products_by_name_from_z_to_a(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.clickOnSortButton();
        page.clickOnSortZAOption();
        assertTrue(productListingPage.areProductsSortedZA(), "Products must be sorted by name Z to A");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void logged_in_user_can_sort_products_by_name_from_a_to_z_after_sorting_from_z_to_a(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.clickOnSortButton();
        page.clickOnSortZAOption();
        page.clickOnSortAZOption();
        assertTrue(productListingPage.areProductsSortedAZ(), "Products must be sorted by name from A to Z");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void logged_in_user_can_sort_products_by_price_from_Lo_to_Hi(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.clickOnSortButton();
        page.clickOnSortLoHiOption();
        assertTrue(productListingPage.areProductsSortedByPriceLoHi(), "Products must be sorted by price Low to High");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void logged_in_user_can_sort_products_by_price_from_Hi_to_Lo(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.clickOnSortButton();
        page.clickOnSortHiLoOption();
        assertTrue(productListingPage.areProductsSortedByPriceHiLo(), "Products must be sorted by price High to Low");
    }

}
