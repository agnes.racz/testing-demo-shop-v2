package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.*;

public class GuestNavigationTest {

    Page page = new Page();
    Header header = new Header();
    LoginModal loginModal = new LoginModal();


    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test
    public void guest_can_navigate_to_cart_page() {
        header.clickOnCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Page title must be: Your cart");
    }

    @Test
    public void guest_can_navigate_to_wishlist_page() {
        header.clickOnWishlistIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Page title must be: Wishlist");
    }

    @Test
    public void guest_can_navigate_to_login_modal() {
        header.clickOnLoginIcon();
        assertEquals(loginModal.getModalTitle(), "Login", "Page title must be: Wishlist");
    }

    @Test
    public void guest_can_navigate_from_cart_page_to_homepage_by_clicking_on_logo() {
        header.clickOnCartIcon();
        header.clickOnLogo();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Your cart");
    }

    @Test
    public void guest_can_navigate_from_wishlist_page_to_homepage_by_clicking_on_logo() {
        header.clickOnWishlistIcon();
        header.clickOnLogo();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Your cart");
    }

    @Test
    public void guest_can_navigate_from_login_modal_to_homepage_by_clicking_on_x() {
        header.clickOnLoginIcon();
        loginModal.clickOnCloseButton();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Your cart");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_navigate_to_product_page(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnProductName();
        assertEquals(page.getPageTitle(), productData.getName(), "Page title must be: " + productData.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_navigate_to_homepage_from_product_page(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnProductName();
        header.clickOnLogo();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: " + productData.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_navigate_to_homepage_from_product_page_by_clicking_on_back_button(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnProductName();
        page.clickOnBackButton();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: " + productData.getName());
    }

}
