package org.fasttrackit;

import org.fasttrackit.dataprovider.ProductDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.util.List;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MinicartBadgeTest {
    Header header = new Header();
    Page page = new Page();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void minicart_badge_is_visible_after_product_is_added_to_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        assertTrue(header.isCartBadgeDisplayed(), "Red badge next to the minicart must be visible.");
    }
    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void minicart_badge_disappears_after_product_is_removed_from_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        header.clickOnCartIcon();
        CartProduct cartProduct = new CartProduct(productData.getId());
        cartProduct.clickOnTrashCanButton();
        assertEquals(header.getNumberOfBadges(),0,  "Number of visible badges must be: 0");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void minicart_badge_shows_1_after_one_product_is_added_to_cart(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        assertEquals(header.getCartBadgeValue(), "1", "Badge must show \"1\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void minicart_badge_shows_3_after_one_product_is_added_to_cart_three_times(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToCartButton();
        productCard.clickOnAddToCartButton();
        productCard.clickOnAddToCartButton();
        assertEquals(header.getCartBadgeValue(), "3", "Badge must show \"3\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "three_products")
    public void minicart_badge_shows_3_after_three_different_products_are_added_to_cart(ProductData productData1, ProductData productData2, ProductData productData3) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToCartButton();
        assertEquals(header.getCartBadgeValue(), "3", "Badge must show \"3\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "fourteen_products")
    public void minicart_badge_shows_14_after_adding_all_products_to_cart(List<ProductData> productData) {
        ProductCard productCard = new ProductCard(productData.get(0).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(1).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(2).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(3).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(4).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(5).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(6).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(7).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(8).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(9).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(10).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(11).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(12).getId());
        productCard.clickOnAddToCartButton();
        productCard = new ProductCard(productData.get(13).getId());
        productCard.clickOnAddToCartButton();
        assertEquals(header.getCartBadgeValue(), "14", "Badge must show \"14\"");
    }

}
