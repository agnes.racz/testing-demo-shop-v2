package org.fasttrackit;

import org.fasttrackit.dataprovider.AccountAndProductDataProvider;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistTest {

    Header header = new Header();
    Page page = new Page();
    WishlistPage wishlistPage = new WishlistPage();
    LoginModal loginModal = new LoginModal();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_badge_is_visible_after_one_product_is_added_to_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertTrue(header.isWishlistBadgeDisplayed(), "Red badge next to the minicart must be visible.");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_badge_shows_1_after_one_product_is_added_to_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "1", "Badge must show \"1\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "three_products")
    public void wishlist_badge_shows_3_after_three_different_products_are_added_to_wishlist(ProductData productData1, ProductData productData2, ProductData productData3) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "3", "Badge must show \"3\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "fourteen_products")
    public void wishlist_badge_shows_14_after_adding_all_products_to_wishlist(List<ProductData> productData) {
        ProductCard productCard = new ProductCard(productData.get(0).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(1).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(2).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(3).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(4).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(5).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(6).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(7).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(8).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(9).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(10).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(11).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(12).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(13).getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "14", "Badge must show \"14\"");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_heart_changes_to_broken_heart_after_product_is_added_to_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertTrue(productCard.isRemoveFromWishlistButtonDisplayed(), "Broken heart icon must be displayed, after product is added to wishlist");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_broken_heart_changes_to_full_heart_after_product_is_removed_from_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        productCard.clickOnRemoveFromWishlistButton();
        assertTrue(productCard.isAddToWishlistButtonDisplayed(), "Full heart icon must be displayed, after product is added to wishlist");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_page_contains_product_added_to_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        assertEquals(wishlistProduct.getName(), productData.getName(), "Product name must be: " + productData.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "three_products")
    public void wishlist_page_contains_3_products_after_three_different_products_are_added_to_wishlist(ProductData productData1, ProductData productData2, ProductData productData3) {
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData1.getId());
        assertEquals(wishlistProduct.getName(), productData1.getName(), "Product name must be: " + productData1.getName());
        wishlistProduct = new WishlistProduct(productData2.getId());
        assertEquals(wishlistProduct.getName(), productData2.getName(), "Product name must be: " + productData2.getName());
        wishlistProduct = new WishlistProduct(productData3.getId());
        assertEquals(wishlistProduct.getName(), productData3.getName(), "Product name must be: " + productData3.getName());
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void guest_can_remove_product_from_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        wishlistProduct.clickOnRemoveFromWishlistButton();
        assertEquals(wishlistPage.getWishlistProductsNumber(), 0, "Number of products in Wishlist must be 0.");
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "product")
    public void wishlist_badge_disappears_after_removing_product_from_wishlist(ProductData productData) {
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        wishlistProduct.clickOnRemoveFromWishlistButton();
        assertEquals(header.getNumberOfBadges(), 0, "Number of visible badges must be 0.");
    }


    //-------------
    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_badge_is_visible_after_logged_in_user_adds_one_product_to_cart(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertTrue(header.isWishlistBadgeDisplayed(), "Red badge next to the minicart must be visible.");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_badge_shows_1_after_logged_in_user_adds_one_product_to_wishlist(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "1", "Badge must show \"1\"");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_three_products")
    public void wishlist_badge_shows_3_after_three_different_products_are_added_to_wishlist_by_logged_in_user(ValidAccount account, ProductData productData1, ProductData productData2, ProductData productData3) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "3", "Badge must show \"3\"");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_fourteen_products")
    public void wishlist_badge_shows_14_after_adding_all_products_to_wishlist_by_logged_in_user(ValidAccount account, List<ProductData> productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.get(0).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(1).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(2).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(3).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(4).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(5).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(6).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(7).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(8).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(9).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(10).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(11).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(12).getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData.get(13).getId());
        productCard.clickOnAddToWishlistButton();
        assertEquals(header.getWishlistBadgeValue(), "14", "Badge must show \"14\"");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_heart_changes_to_broken_heart_after_product_is_added_to_wishlist_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        assertTrue(productCard.isRemoveFromWishlistButtonDisplayed(), "Broken heart icon must be displayed, after product is added to wishlist");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_broken_heart_changes_to_full_heart_after_product_is_removed_from_wishlist_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        productCard.clickOnRemoveFromWishlistButton();
        assertTrue(productCard.isAddToWishlistButtonDisplayed(), "Full heart icon must be displayed, after product is added to wishlist");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_page_contains_product_added_to_wishlist_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        assertEquals(wishlistProduct.getName(), productData.getName(), "Product name must be: " + productData.getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_three_products")
    public void wishlist_page_contains_3_products_after_three_different_products_are_added_to_wishlist_by_logged_in_user(ValidAccount account, ProductData productData1, ProductData productData2, ProductData productData3) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData1.getId());
        assertEquals(wishlistProduct.getName(), productData1.getName(), "Product name must be: " + productData1.getName());
        wishlistProduct = new WishlistProduct(productData2.getId());
        assertEquals(wishlistProduct.getName(), productData2.getName(), "Product name must be: " + productData2.getName());
        wishlistProduct = new WishlistProduct(productData3.getId());
        assertEquals(wishlistProduct.getName(), productData3.getName(), "Product name must be: " + productData3.getName());
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void logged_in_user_can_remove_product_from_wishlist(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        wishlistProduct.clickOnRemoveFromWishlistButton();
        assertEquals(wishlistPage.getWishlistProductsNumber(), 0, "Number of products in Wishlist must be 0.");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_product")
    public void wishlist_badge_disappears_after_removing_product_from_wishlist_by_logged_in_user(ValidAccount account, ProductData productData) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData.getId());
        wishlistProduct.clickOnRemoveFromWishlistButton();
        assertEquals(header.getNumberOfBadges(), 0, "Number of visible badges must be 0.");
    }

    @Test(dataProviderClass = AccountAndProductDataProvider.class, dataProvider = "account_and_three_products")
    public void logged_in_user_can_remove_product_from_wishlist_after_adding_three_products_to_wishlist(ValidAccount account, ProductData productData1, ProductData productData2, ProductData productData3) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        ProductCard productCard = new ProductCard(productData1.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData2.getId());
        productCard.clickOnAddToWishlistButton();
        productCard = new ProductCard(productData3.getId());
        productCard.clickOnAddToWishlistButton();
        header.clickOnWishlistIcon();
        WishlistProduct wishlistProduct = new WishlistProduct(productData1.getId());
        wishlistProduct.clickOnRemoveFromWishlistButton();
        assertEquals(wishlistPage.getWishlistProductsNumber(), 2, "Number of products in Wishlist must be 2.");
    }
}

