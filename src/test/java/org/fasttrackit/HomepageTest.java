package org.fasttrackit;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class HomepageTest {

    Page page = new Page();

    @Test
    public void user_can_open_homepage(){
        page.openHomepage();
        assertEquals(page.getPageTitle(), "Products", "Page title must be: Products");
    }
}
