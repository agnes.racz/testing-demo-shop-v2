package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class ProductDataProvider {
    static ProductData rfm = new ProductData("0", "Refined Frozen Mouse", "9.99");
    static ProductData agc = new ProductData("1", "Awesome Granite Chips", "15.99");
    static ProductData hmt = new ProductData("2", "Home Made Tower", "25.19");
    static ProductData ich = new ProductData("3", "Incredible Concrete Hat", "7.99");
    static ProductData amc = new ProductData("4", "Awesome Metal Chair", "15.99");
    static ProductData pwb = new ProductData("5", "Practical Wooden Bacon", "29.99");
    static ProductData ass = new ProductData("6", "Awesome Soft Shirt", "29.99");
    static ProductData ppb = new ProductData("7", "Practical Plastic Bacon", "1.99");
    static ProductData pmm = new ProductData("8", "Practical Metal Mouse", "9.99");
    static ProductData lsg = new ProductData("9", "Licensed Steel Gloves", "14.99");
    static ProductData gsp = new ProductData("10", "Gorgeous Soft Pizza", "19.99");
    static ProductData cp = new ProductData("11", "Cheese Pizza", "99.09");
    static ProductData rh = new ProductData("12", "Rocker Hummer", "299");
    static ProductData ql = new ProductData("13", "Quis Lectus", "169");

    @DataProvider(name = "product")
    public Object[][] getProduct() {

        return new Object[][]{
                {rfm}, {agc}, {hmt}, {ich}, {amc}, {pwb}, {ass},
                {ppb}, {pmm}, {lsg}, {gsp}, {cp}, {rh}, {ql}
        };
    }

    @DataProvider(name = "three_products")
    public Object[][] getThreeProducts() {

        return new Object[][]{
                {rfm, agc, hmt},
                {hmt, ich, amc},
                {ich, ppb, ql},
                {pwb, ass, pmm},
                {lsg, gsp, cp},
                {rh, agc, amc}
        };
    }

    @DataProvider(name = "two_products")
    public Object[][] getTwoProducts() {

        return new Object[][]{
                {agc, amc},
                {amc, ass},
                {ass, cp},
                {cp, gsp},
                {gsp, hmt},
                {hmt, ich},
                {ich, lsg},
                {lsg, pmm},
                {pmm, ppb},
                {ppb, pwb},
                {pwb, ql},
                {ql, rfm},
                {rfm, rh},
                {rh, agc},
        };
    }

    @DataProvider(name = "fourteen_products")
    public Object[][] getFourteenProducts() {
        List<ProductData> products = new ArrayList<>();
        products.add(rfm);
        products.add(agc);
        products.add(hmt);
        products.add(ich);
        products.add(amc);
        products.add(pwb);
        products.add(ass);
        products.add(ppb);
        products.add(pmm);
        products.add(lsg);
        products.add(gsp);
        products.add(cp);
        products.add(rh);
        products.add(ql);

        return new Object[][]{
                {products}
        };
    }
}
