package org.fasttrackit.dataprovider;

import org.fasttrackit.SearchQueryData;
import org.testng.annotations.DataProvider;

public class SearchQueryDataProvider {
    static SearchQueryData awesome = new SearchQueryData("awesome", 3);

    static SearchQueryData metal = new SearchQueryData("metal", 2);

    static SearchQueryData bacon = new SearchQueryData("bacon", 2);

    static SearchQueryData pizza = new SearchQueryData("pizza", 2);

    static SearchQueryData cloud = new SearchQueryData("cloud", 0);
    static SearchQueryData stone = new SearchQueryData("stone", 0);
    static SearchQueryData ocean = new SearchQueryData("ocean", 0);
    static SearchQueryData number = new SearchQueryData("1", 0);



    @DataProvider(name = "searchQuery")
    public Object[][] getSearchQuery() {

        return new Object[][]{
                {awesome}, {metal}, {bacon}, {pizza}
        };
    }

    @DataProvider(name = "invalidSearchQuery")
    public Object[][] getInvalidSearchQuery() {

        return new Object[][]{
                {cloud}, {stone}, {ocean}, {number}
        };
    }

}
