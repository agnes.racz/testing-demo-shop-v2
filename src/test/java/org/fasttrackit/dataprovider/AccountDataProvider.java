package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidAccount;
import org.fasttrackit.ValidAccount;
import org.testng.annotations.DataProvider;

public class AccountDataProvider {

    static ValidAccount beetle = new ValidAccount("beetle", "choochoo");
    static ValidAccount dino = new ValidAccount("dino", "choochoo");
    static ValidAccount ducker = new ValidAccount("ducker", "choochoo");
    static ValidAccount turtle = new ValidAccount("turtle", "choochoo");
    static InvalidAccount locked = new InvalidAccount("locked", "choochoo", "The user has been locked out.");
    static InvalidAccount noUserNoPass = new InvalidAccount("", "", "Please fill in the username!");
    static InvalidAccount valUserNoPass = new InvalidAccount("dino", "", "Please fill in the password!");
    static InvalidAccount valUserInvalPass = new InvalidAccount("dino", "abc", "Incorrect username or password!");
    static InvalidAccount invalUserInvalPass = new InvalidAccount("abc", "abc", "Incorrect username or password!");
    static InvalidAccount noUserValPass = new InvalidAccount("", "choochoo", "Please fill in the username!");
    static InvalidAccount invalUserValPass = new InvalidAccount("abc", "choochoo", "Incorrect username or password!");

    @DataProvider(name = "valid_account")
    public Object[][] getValidAccount() {

        return new Object[][]{
                {beetle},
                {dino},
                {ducker},
                {turtle}
        };
    }
    @DataProvider(name = "invalid_account")
    public Object[][] getInvalidAccount() {

        return new Object[][]{
                {locked},
                {noUserValPass},
                {noUserNoPass},
                {valUserInvalPass},
                {valUserNoPass},
                {invalUserInvalPass},
                {invalUserValPass}
        };
    }
}
