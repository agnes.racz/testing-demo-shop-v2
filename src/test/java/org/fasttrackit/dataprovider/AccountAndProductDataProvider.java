package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

import static org.fasttrackit.dataprovider.AccountDataProvider.*;
import static org.fasttrackit.dataprovider.ProductDataProvider.*;

public class AccountAndProductDataProvider {

    @DataProvider(name = "account_and_product")
    public Object[][] getAccountAndProduct() {

        return new Object[][]{
                {beetle, rfm}, {beetle, agc}, {beetle, hmt}, {beetle, ich}, {beetle, amc},
                {dino, pwb}, {dino, ass}, {dino, ppb}, {dino, pmm}, {dino, lsg},
                {turtle, gsp}, {turtle, cp}, {turtle, rh}, {turtle, ql}, {turtle, rfm}, {turtle, agc},
                {ducker, hmt}, {ducker, ich}, {ducker, amc}, {ducker, pwb}, {ducker, ass},
        };
    }

    @DataProvider(name = "account_and_two_products")
    public Object[][] getAccountAndTwoProducts() {

        return new Object[][]{
                {beetle, rfm, agc}, {beetle, agc, hmt}, {beetle, hmt, ich}, {beetle, ich, amc},
                {dino, pwb, ass}, {dino, ass, ppb}, {dino, ppb, pmm}, {dino, pmm, lsg},
                {turtle, gsp, cp}, {turtle, cp, rh}, {turtle, rh, ql}, {turtle, ql, rfm},
                {ducker, hmt, ich}, {ducker, ich, amc}, {ducker, amc, pwb}, {ducker, pwb, ass}
        };
    }

    @DataProvider(name = "account_and_three_products")
    public Object[][] getAccountAndThreeProducts() {

        return new Object[][]{
                {beetle, rfm, agc, hmt}, {beetle, ich, amc, pwb},
                {dino, pwb, ass, ppb}, {dino, ass, pmm, lsg},
                {turtle, gsp, cp, rh}, {turtle, ql, rfm, agc},
                {ducker, hmt, ich, amc}, {ducker, pwb, ass, ql}
        };
    }

    @DataProvider(name = "account_and_fourteen_products")
    public Object[][] getAccountAndFourteenProducts() {
        List<ProductData> products = new ArrayList<>();
        products.add(rfm);
        products.add(agc);
        products.add(hmt);
        products.add(ich);
        products.add(amc);
        products.add(pwb);
        products.add(ass);
        products.add(ppb);
        products.add(pmm);
        products.add(lsg);
        products.add(gsp);
        products.add(cp);
        products.add(rh);
        products.add(ql);

        return new Object[][]{
                {beetle, products},
                {dino, products},
                {turtle, products},
                {ducker, products}
        };
    }
}
