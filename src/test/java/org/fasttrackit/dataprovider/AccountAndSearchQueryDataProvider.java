package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

import static org.fasttrackit.dataprovider.AccountDataProvider.*;
import static org.fasttrackit.dataprovider.AccountDataProvider.ducker;
import static org.fasttrackit.dataprovider.SearchQueryDataProvider.*;

public class AccountAndSearchQueryDataProvider {

        @DataProvider(name = "account_and_searchQuery")
        public Object[][] getAccountAndProduct() {

            return new Object[][]{
                    {beetle, pizza}, {beetle, bacon}, {beetle, metal}, {beetle, awesome},
                    {dino, pizza}, {dino, bacon}, {dino, metal}, {dino, awesome},
                    {turtle, pizza}, {turtle, bacon}, {turtle, metal}, {turtle, awesome},
                    {ducker, pizza}, {ducker, metal}, {ducker, bacon}, {ducker, awesome}
            };
        }
}
