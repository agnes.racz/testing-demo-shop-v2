package org.fasttrackit;

import org.fasttrackit.dataprovider.AccountAndSearchQueryDataProvider;
import org.fasttrackit.dataprovider.SearchQueryDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTest {

    Page page = new Page();
    Header header = new Header();
    LoginModal loginModal = new LoginModal();
    SearchResultPage searchResultPage = new SearchResultPage();


    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = SearchQueryDataProvider.class, dataProvider = "searchQuery")
    public void search_functionality_returns_correct_amount_of_products_in_case_of_valid_query(SearchQueryData query) {
        page.typeInSearchQuery(query.getSearchQuery());
        page.clickOnSearchButton();
        assertEquals(searchResultPage.getReturnedProductsNumber(),query.getExpectedProductsNumber(), "Expected products number must be: " + query.getExpectedProductsNumber());
    }

    @Test(dataProviderClass = SearchQueryDataProvider.class, dataProvider = "searchQuery")
    public void every_search_results_contains_the_search_query(SearchQueryData query) {
        page.typeInSearchQuery(query.getSearchQuery());
        page.clickOnSearchButton();
        assertTrue(searchResultPage.areResultsContainingQuery(query.getSearchQuery()),"Returned products name must contain: " + query.getSearchQuery());
    }
    @Test(dataProviderClass = SearchQueryDataProvider.class, dataProvider = "searchQuery")
    public void search_functionality_returns_correct_amount_of_products_in_case_of_valid_query_by_hitting_Enter(SearchQueryData query) {
        page.typeInSearchQuery(query.getSearchQuery());
        page.hitEnterOnSearchButton();
        assertEquals(searchResultPage.getReturnedProductsNumber(),query.getExpectedProductsNumber(), "Expected products number must be: " + query.getExpectedProductsNumber());
    }
    @Test(dataProviderClass = SearchQueryDataProvider.class, dataProvider = "invalidSearchQuery")
    public void search_functionality_returns_0_products_in_case_of_invalid_query(SearchQueryData query) {
        page.typeInSearchQuery(query.getSearchQuery());
        page.clickOnSearchButton();
        assertEquals(searchResultPage.getReturnedProductsNumber(),query.getExpectedProductsNumber(), "Expected products number must be: " + query.getExpectedProductsNumber());
    }
    @Test(dataProviderClass = AccountAndSearchQueryDataProvider.class, dataProvider = "account_and_searchQuery")
    public void search_functionality_returns_correct_amount_of_products_in_case_of_valid_query_and_logged_in_user(ValidAccount account, SearchQueryData query) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        page.typeInSearchQuery(query.getSearchQuery());
        page.clickOnSearchButton();
        assertEquals(searchResultPage.getReturnedProductsNumber(),query.getExpectedProductsNumber(), "Expected products number must be: " + query.getExpectedProductsNumber());
    }



}
