package org.fasttrackit;

import org.fasttrackit.dataprovider.AccountDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTest {

    Page page = new Page();
    Header header = new Header();
    LoginModal loginModal = new LoginModal();

    @BeforeMethod
    public void openHomepage() {
        page.openHomepage();
    }

    @AfterMethod
    public void closeSession() {
        closeWebDriver();
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void user_can_login_with_valid_credentials(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        assertEquals(header.getGreetingsMessage(), account.getGreetingMessage(), "Greetings message must be: Hi " + account.getUsername() + "!");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "valid_account")
    public void login_icon_changes_to_log_out_icon_after_successful_login(ValidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        assertTrue(header.isLogOutIconDisplayed(),"Log out button must be displayed");
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "invalid_account")
    public void right_error_message_appears_in_case_of_invalid_login_credentials(InvalidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        assertEquals(loginModal.getErrorMessage(), account.getErrorMessage(), "Error message must be: " + account.getErrorMessage());
    }

    @Test(dataProviderClass = AccountDataProvider.class, dataProvider = "invalid_account")
    public void guest_can_close_the_login_modal_in_case_of_failed_login(InvalidAccount account) {
        header.clickOnLoginIcon();
        loginModal.typeInUsername(account.getUsername());
        loginModal.typeInPassword(account.getPassword());
        loginModal.clickOnLoginButton();
        loginModal.clickOnCloseButton();
        header.clickOnCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Guest must be able to close login modal and continue to Cart page");
    }

}
